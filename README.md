# MATLABrepo - KOM1012

**YTU KOM1012 2020 SPRING**


master/Intro/
```
>problem_Set1(lucun)[EN] >>> .m/.html
                - Variables, Vectors, Matrices and Array Operations
                    >16 Problems and solutions
                    
>problem_Set2_3(lucun)[EN] >>> .m/.html
                -m-files, Input&Output, Selection Structures
                    >9 Problems and solutions
                    
>problem_Set4(lucun)[EN] >>> .m/.html
                -Repetition Structures
                    >5 Problems and solutions
                    
problem_Set5(lucun)[EN] >>> .m/.html
                -Problem solving with functions..
                    >3 Problems,solutions and functions
                        function files of problemSet:
                            ebob.m
                            ekok.m
                            mex_value.m
                            
problem_Set6(lucun)[EN] >>> .m/.html
                -Functions
                    >5 Problems,solutions and functions
                        function files of problemSet:
                            my_cos.m
                            my_exp.m
                            my_quad.m
                            store_func.m
                            faktoriyel.m
                            
problem_Set7(lucun)[EN] >>> .m/.html
                -2D Ploting..
                    >5 Problems,solutions and graphs..
                    
>problem_Set8(lucun)[EN] >>> .m/.html
                -Arrays
                    >7 Problems and solutions
                    
>problem_Set9(lucun)[EN] >>> .m/.html
                -Reading & Writing Data from Files-Symbolic
                    >11 Problems and solutions
                    
>problem_Set10(lucun)[EN] >>> .m/.html
                -Graphical User Interface
                    >2 Problems, solutions and .fig files
                    
>problem_Set11(lucun)[EN] >>> .m/.html
                -Simulink
                    >4 Problems, models and .slx files
____________________________________________________________________________
                    
>firstDayz(youtube)[TR] >>> .m/.html
        matrislerde işlemler>>
                -skaler işlemler
        vektör komutları>>
                -linspace()                 #lineer aralıklı vektör oluşturma
                -logspace()                 #logaritmik aralıklı vektör oluşturma
                -sum()                      #vektör elemanları toplamı
                -mean()                     #vektör elemanları ortamalası
                -length()                   #vektör uzunluğu
                -min/max()                  #enBüyük-enKüçük elemanlar
                -prod()                     #vektör elemanlarının birbiriyle çarpımı 
                -sign()                     #işaret tayini
                -fix/floor/ceil/round()     #yuvarlama fonskiyonları
                -sort()                     #sıralama fonskiyonu
                
                
>week1_Notes(lucun)[EN] >>> .m/.html
                -Relational Operators
                -Logical Operators
                -Arithmetic Operators (for scalars)
                -Special Cases and Functions
                -Logical Functions
                -using Colon Operator:
                -linspace() and logspace() functions:
                -MATRICES
                -Matrix built-in funcs
                -Arithmetic Operators (for arrays)
                -submatrices
                -constant and permanent variables in MATLAB
                
>week2_Notes(lucun)[EN] >>> .m/.html
                Elementary Math Functions
                Complex Number Functions
                Trigonometric Functions
                Exponential Functions
                Polynomial Functions
                Matrix Operation Functions
                Output Options
                

```
**directory tree**:
```
master:.
│   
├───README.md
│
└───Files
    │  
    ├───notes
    │       firstDayz.html
    │       firstDayz.m
    │       week1.html
    │       week1.m
    │       week2.html
    │       week2.m
    │
    ├───problem_Set01
    │       problem_Set1.html
    │       problem_Set1.m
    │
    ├───problem_Set02-03
    │       problem_Set2_3.html
    │       problem_Set2_3.m
    │
    ├───problem_Set04
    │       problem_Set4.html
    │       problem_Set4.m
    │
    ├───problem_Set05
    │       ebob.m
    │       ekok.m
    │       max_value.m
    │       problem_Set5.html
    │       problem_Set5.m
    │
    ├───problem_Set06
    │       faktoriyel.m
    │       my_cos.m
    │       my_exp.m
    │       my_quad.m
    │       problem_Set6.html
    │       problem_Set6.m
    │       store_func.m
    │
    ├───problem_Set07
    │   │   problem_Set7.m
    │   │
    │   └───html
    │           problem_Set7.html
    │
    │
    ├───problem_Set08
    │       problem_Set8.html
    │       problem_Set8.m
    │
    ├───problem_Set09
    │   │   problem_Set9.m
    │   │   prop_metal.xls
    │   │
    │   └───html
    │           problem_Set9.html
    │    
    │
    ├───problem_Set10
    │   │   adding_machine.fig
    │   │   adding_machine.m
    │   │   basic_calculator.fig
    │   │   basic_calculator.m
    │   │
    │   └───html
    │           all.html
    │            
    │    
    └───problem_Set11
            pset10_1.slx
            pset10_2.slx
            pset10_3.slx
            pset10_4.slx
            rexi.html            

                

```