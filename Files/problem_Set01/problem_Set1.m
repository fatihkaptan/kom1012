%% problem_Set #1 
%
%url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Sat Feb 22 20:15:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%% 1.
clear all
clc
format short

%{
Give values to variables a and b on the command line, e.g.., a=3 and b=5.
Write some statements to find the sum, difference, product, and quotient of a and b.
--quotient:oran
%}

a=20
b=5
fprintf("a+b: %d\n",a+b)
fprintf("a-b: %d\n",a-b)
fprintf("a*b: %d\n",a*b)
fprintf("a/b: %d\n",a/b)

%% 2. 
clear all
clc
%{
Assign a value to the variable x on the command line, e.g., x = 4 ? ?. 
What is the square root of x? 
What is the cosine of the square root of x?
%}
x= 10 * pi
fprintf("square root of x = %f \n",sqrt(x))
fprintf("cosine of the square root of x = %f\n",cos(sqrt(x)))

%% 3.
clear all
clc

%{
Assign a value to the variable y on the command line as follows: 
y = ?1. 
What is the square root of y?
%}

y=-1
% y1 = sqrt(y);
% fprintf("square root of -1 = %f\n",sqrt(y))
disp("square root of -1 = "+sqrt(y))
class(sqrt(y))

%% 4.
clear all
clc

%{
Enter a statement like:
                        x = [1 3 0 ?1 5].
i. Can you see that you have created a vector (list) with five elements?
ii. Enter the command disp(x) to see how MATLAB displays a vector.
%}

x = [ 1 2 3 4 5]
disp(x)
% fprintf("%d ",x)
% class(x)

%% 5.
clear all
clc

%{
You can use one vector in a list for another one. Type in the following:
a = [1 2 3];
b =[4 5];
c =[a -b];
i. Can you work out what c will look like before displaying it?
ii. And what about this?
a = [1 3 7];
a =[a 0 -1];
%}

a = 1:3
b = 4:5
c = [a -b]

a2 = [1 3 7]
a2 = [a2 0 -1]

%% 6.
clear all
clc
format rat
%{
Evaluate the following MATLAB expressions yourself before checking the answers in MATLAB:
1 + 2 * 3
4/2 * 2
1 + 2/4
1 + 2\4
2 * 2�3
2 * 3\3
2�(1 + 2)/3
1/2e - 1
%}

%operator precedence documentation: https://www.mathworks.com/help/matlab/matlab_prog/operator-precedence.html

1+2*3
4/2*2
1+2/4
1+2\4
2 * 2^3
2*3\3
2^(1+2)/3
1/2*exp(1) - 1

%format rat = show rational

%% 7.
clear all
clc
%{
Use MATLAB to evaluate the following expressions.
i. 1/(2�3)
ii. 2^(2�3)
iii. 1.5 � 10^-4 + 2.5 � 10^-2
%}

1/(2*3)
2^(2*3)
(1.5 * 10^-4)  + (2.5 * 10^-2)

%% 8.
clear all
clc
format short

%{
Use MATLAB array operations to do the following:
i. Add 1 to each element of the vector [2 3 -1].
ii. Multiply each element of the vector [1 4 8] by 3.
iii. Find the array product of the two vectors [1 2 3] and [0 -1 1].
iv. Square each element of the vector [2 3 1].

%}

v1 = [2 3 -1];
1 + v1

v2 = [1 4 8];
v2 .* v2 

v3 = 1:3;, v4 = [0 -1 1];
v3 .* v4

v5 = [2 3 1];
v5.^2


%% 9.
clear all
clc

%{
Use MATLAB to evaluate the following expressions.
i. sqrt(2)
ii. (3+4)/(5+6)
iii. Find the sum of 5 and 3 divided by their product
iv. 2^3^2
v. Find the square of 2*pi
vi. 2*pi^2
vii. 1/sqrt(2*pi)
viii. 1/(2sqrt(pi))
ix. Find the cube root of the product of 2.3 and 4.5
x. (1? 2/(3+2))/(1+2/(3-2))
xi. 1000(1 + 0.15/12)^60
xii. (0.0000123 + 5.678 � 10^?3) * 0.4567 * 10^-4

%}

disp("i. sqrt(2) : "+sqrt(2)) 
disp("ii. (3+4)/(5+6) : "+(3+4)/(5+6))
disp("iii. Find the sum of 5 and 3 divided by their product : "+(3+5)/(3*5))
disp("iv. 2^3^2 : "+(2^3^2))
disp("v.  Find the square of 2*pi : "+((2*pi)^2))
disp("vi. 2*pi^2 : "+(2*pi^2))
disp("vii. 1/sqrt(2*pi)  : "+(1/sqrt(2*pi)))
disp("viii. 1/(2*sqrt(pi)) : "+(1/(2*sqrt(pi))))
disp("ix. Find the cube root of the product of 2.3 and 4.5 : "+((2.3*4.5)^(1/3)))
%Two simple options:
% x^(1/3)
% Or,
% nthroot(x,3)
disp("x. (1- 2/(3+2))/(1+2/(3-2)) : "+(1- 2/(3+2))/(1+2/(3-2)))
disp("xi. 1000(1 + 0.15/12)^60 : "+1000*(1 + 0.15/12)^60)
disp("xii. (0.0000123 + 5.678 � 10^-3) * 0.4567 * 10^-4 : "+(0.0000123 + 5.678 * 10^-3) * 0.4567 * 10^-4)

%% 10.
clear all
clc
format rat

%{
Set up a vector n with elements 1, 2, 3, 4, 5. 
Use MATLAB array operations on it to set up the following four vectors, 
each with five elements:

i. 2, 4, 6, 8, 10
ii. 1/2, 1, 3/2, 2, 5/2
iii. 1, 1/2, 1/3, 1/4, 1/5
iv. 1, 1/2^2, 1/3^2, 1/4^2, 1/5^2

%}

n = 1:5;

2.*n        %i.
n./2        %ii.
1./n        %iii.
1./(n.^2)   %iv.

%% 11.
clear all
clc

%{
The following statements all assign logical expressions to the variable x. 
See if you can correctly determine the value of x in each case before checking your answer with MATLAB.

i. x = 3 > 2
ii. x = 2 > 3
iii. x = -4 <= -3
iv. x = 1 < 1
v. x = 2 ~= 2
vi. x = 3 == 3
vii. x = 0 < 0.5 < 1

%}

x = 3 > 2
x = 2 > 3
x = -4 <= -3
x = 1 < 1
x = 2 ~= 2
x = 3 == 3
x = 0 < 0.5 < 1 %!!


%% 12.
clear all
clc

%{
MATLAB contains functions to calculate the natural logarithm (log), 
the logarithm to the base 10 (log10 ), 
and the logarithm to the base 2 (log2 ). 
However, if you want to find a logarithm to another base -for example, 
base 'b' you'll have to do the math yourself with the formula
logb(x) = (loge(x))/(loge(b))
What is the logb of 10 when b is defined from 1 to 10 in increments of 1?

%}
log10(10);
log2(4);
% b= 1:10;
% b = input("logb of 10, pls input b:  ") % base = b
b = 1;
log1_base1 = log(10)/log(b)

b = 2;
log10_base2 = log(10)/log(b)

b = 5;
log10_base5 = log(10)/log(b)

b = 8;
log10_base8 = log(10)/log(b)

b = 10;
log10_base10 = log(10)/log(b)


%% 13.
clear all
clc
format short

%{
The displacement of the oscillating spring can be described by
                    x(t) = A cos(wt)
where x is the displacement at time t, A is the maximum displacement, 
w is the angular frequency which depends on the spring constant and 
the mass attached to the spring, and t is the time.
Find the displacement x for times from 0 to 10 seconds when the maximum 
displacement A is 4cm, and the angular frequency is 0.6rad/s. 
Present your results in a table array of displacement and time values.

%}

A = 4; 
w = 0.6;
t = 0:10; 

table = [t ; A*cos(w*t)]'
 

%% 14.
clear all
clc

%{
Consider the following table of data representing temperature 
readings in a reactor and use MATLAB to find;

i. The maximum temperature measured by each thermocouple.
ii. The minimum temperature measured by each thermocouple.
iii. The average temperature measured by each thermocouple.

Thermocouple 1 Thermocouple 2 Thermocouple 3
84.3            90.0            86.7
86.4            89.5            87.6
85.2            88.6            88.3
87.1            88.9            85.3
83.5            88.9            80.3
84.8            90.4            82.4
85.0            89.3            83.4
85.3            89.5            85.4
85.3            88.9            86.3
85.2            89.1            85.3
82.3            89.5            89.0
84.7            89.4            87.3
83.6            89.8            87.2
%}

t1 = [82 83 85 80 82 84.5]; %girdiler sallama
t2 = [90 91 90.1 90.5 90.8];
t3 = [88 83 86 89 85.6 83.9];

disp("termo_1: ")
disp("max: "+max(t1)+" min: "+min(t1)+" avarage: "+mean(t1))
disp("termo_2: ")
disp("max: "+max(t2)+" min: "+min(t2)+" avarage: "+mean(t2))
disp("termo_3: ")
disp("max: "+max(t3)+" min: "+min(t3)+" avarage: "+mean(t3))

%% 15.
clear all
clc

%{
Create the following matrices, and use them in the exercises that follow:

a = [15 3 22 ; 3  8 5 ; 14 3 82]
b =[1;5;6]
c =[12 18 5 2]

i. Create a matrix called d from the third column of matrix a.
ii. Combine matrix b and matrix d to create matrix e, 
a two-dimensional matrix with three rows and two columns.
iii. Combine matrix b and matrix d to create matrix f , 
a one-dimensional matrix with six rows and one column.
iv. Create a matrix g from matrix a and the first three elements of matrix c,
with four rows and three columns.
v. Create a matrix h with the first element equal to a(1,3), 
the second element equal to c(1,2) and the third element equal to b(2,1) .

%}
a = [15 3 22 ; 3  8 5 ; 14 3 82];
b =[1;5;6];
c =[12 18 5 2];

disp("i.")
d = a(:,3)

disp("ii.")
e = [b d]

disp("iii.")
f = [b;d] 

disp("iv.")
g= [a ;c(1:3)]

disp("v.")
h= [a(1,3),c(1,2),b(2,1)]


%% 16.
clear all
clc

%{
Compute the matrix product A * B of the following pairs of matrices, 
and show that A * B is not the same as B * A.
i. A = [12 4 ; 3 -5] B = [2 12 ; 0 0]
ii. A = [1 3 5 ; 2 4 6] B = [-2 4 ; 3 8 ; 12 -2]

%}

A = [12 4 ; 3 -5];, B = [2 12 ; 0 0];
A*B
% A.*B
(A*B) == (B*A)

A2 = [1 3 5 ; 2 4 6];,B2 = [-2 4 ; 3 8 ; 12 -2];
sonuc2 = A2*B2
sonuc2_2 = B2*A2
size(sonuc2), size(sonuc2_2) 
size(sonuc2) == size(sonuc2_2)
% A2*B2 ~= B2*A2

%% notes


%format rat = show rational , short(4 digit) long(15~ digit)

%operator precedence documentation: https://www.mathworks.com/help/matlab/matlab_prog/operator-precedence.html

% nthroot(n,which_root) or n^(1/x)

%size(matrix) = give dimension of matrix



