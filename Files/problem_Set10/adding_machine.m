%%% problem_Set #10
%
% Graphical User Interface
%
%README : 
%       >gui programs don't run without *.fig files, download from mainpage matlab dropdownmenu
%       >I deleted default comment lines whick matlab created automaticly
%       >in adding machine I add just 6 command. (to edit1,2 callback and pushbuton callback)
%url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Fri Jun 12 167:25:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%if include input funciton. publish html outputs doesn't work!
%
%gui programs don't run without *.fig files, download from
%% 1. Adding machine 
function varargout = adding_machine(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @adding_machine_OpeningFcn, ...
                   'gui_OutputFcn',  @adding_machine_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end


function adding_machine_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);
function varargout = adding_machine_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)

handles.sayi2=str2double(get(hObject,'String')) %one
guidata(hObject, handles); %two


function edit1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)

handles.sayi1=str2double(get(hObject,'String')) %three
guidata(hObject, handles); %four

function edit2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton1_Callback(hObject, eventdata, handles)

handles.sonuc = handles.sayi1+handles.sayi2; %five
set(handles.text3,'string',handles.sonuc); %six
 
%% 2. Calculator
