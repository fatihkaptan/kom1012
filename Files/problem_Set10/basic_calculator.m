
function varargout = basic_calculator(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @basic_calculator_OpeningFcn, ...
                   'gui_OutputFcn',  @basic_calculator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end


function basic_calculator_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);
function varargout = basic_calculator_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;



function sayi2_Callback(hObject, eventdata, handles)

handles.sayi2=str2double(get(hObject,'String')) 
guidata(hObject, handles); 


function sayi2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sayi1_Callback(hObject, eventdata, handles)

handles.sayi1=str2double(get(hObject,'String')) 
guidata(hObject, handles); 

function sayi1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function esittir_Callback(hObject, eventdata, handles)


function toplama_Callback(hObject, eventdata, handles)

handles.sonuc = handles.sayi1+handles.sayi2;
set(handles.ekran,'string',handles.sonuc); 

function cikarma_Callback(hObject, eventdata, handles)

handles.sonuc = handles.sayi1-handles.sayi2;
set(handles.ekran,'string',handles.sonuc); 


function carpma_Callback(hObject, eventdata, handles)

handles.sonuc = handles.sayi1*handles.sayi2;
set(handles.ekran,'string',handles.sonuc); 

function bolme_Callback(hObject, eventdata, handles)

handles.sonuc = handles.sayi1/handles.sayi2;
set(handles.ekran,'string',handles.sonuc); 

