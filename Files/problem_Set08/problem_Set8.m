%% problem_Set #8 
%
% Arrays
% 
% url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Mon Apr 20 01:02:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%if include input funciton. publish html outputs doesn't work!
%% 1.
clear all
clc

%{
Calculate the sum (not the partial sums) of the first 10 milion terms 
in the harmonic series;
1 + 1/2 + 1/3 + 1/4 + 1/5 + ... 1/n ...

using both doube-precision and single-precision numbers. 
Compare the results. Explain why they are different

%}

toplam = 0;
single_toplam=0;

for i=1:10000000
    toplam = toplam + 1/i;
end

for j=1:single(10000000)
    single_toplam = single_toplam + 1/j;
end

fprintf('double precision number : %f\n',toplam)
fprintf('single precision number : %f\n',single_toplam)


%% 2.
clear all
clc

%{
Complex numbers are automatically created in MATLAB as a result of calculations.
They can also be entered directly, as the addition of a real and 
an imaginary number,and can be stored as any of the numeric data types. 
Define two variables: a single and a double-precision complex number, as
                doublea = 5 + 3 i
                singlea = single(5 + 3 i)
Raise each of these numbers to the 100th power. 
Explain the difference in your answers.

%}

doublea = 5 + 3i;
singlea = single(5 + 3i);

double_a_100th_power = doublea^100
single_a_100th_power = singlea^100

double_a_100th_power == single_a_100th_power
%?

%% 3.
clear all
clc

%{
Sometimes it is confusing to realize that numbers can be represented as 
both numeric data and character data. 
Use MATLAB to express the number 85 as a character array.
a. How many elements are in this array?
b. What is the numeric equivalent of the character 8?
c. What is the numeric equivalent of the character 5?

%}
% data = char(85); %?

data_a = '85';
% double(data_a)
length_of_array = length(data_a)

b= '8';
b_ascii = double(b)

c = '5';
c_ascii = double(c)
%% 4.
clear all
clc

%{
Create each of the following arrays:
A =[1 2;3 4] 
B =[10 20;30 40]
C =[3 16;9 12]
a. Combine them into one large 2 � 2 � 3 multidimensional array called ABC.
b. Extract each column 1 into a 2 � 3 array called Column_A1B1C1.
c. Extract each row 2 into a 3 � 2 array called Row_A2B2C2 .
d. Extract the value in row 1, column 2, page 3.
%}

%a.
A =[1 2;3 4];
B =[10 20;30 40];
C =[3 16;9 12];

my_3d_array(:,:,1) = A;
my_3d_array(:,:,2) = B;
my_3d_array(:,:,3) = C;
my_3d_array
% ar = my_3d_array;
% ar(1,1,1)
% ar(2,2,3)
% ar(2,2,2)
%or
% cat(3,A,B,C) %cat means concatenate array to n dimensional matrix

% see this 3d array here:
% https://gitlab.com/fatihkaptan/fatihkaptan.gitlab.io/-/raw/master/depo/3d_array_1977.jpg

Column_A1B1C1 = my_3d_array(:,1,:)
Row_A2B2C2 = my_3d_array(2,:,:)
value123 = my_3d_array(1,2,3) 



%xxx false but true..xxx
% %b.
% my_2d_array(:,1) = A(:,1);
% my_2d_array(:,2) = B(:,1);
% my_2d_array(:,3) = C(:,1);
% my_2d_array
% %c.
% my_2d_array2(1,:) = A(2,:);
% my_2d_array2(2,:) = B(2,:);
% my_2d_array2(3,:) = C(2,:);
% my_2d_array2
% %d.
%..


%% 5.
clear all
clc

%{
Imagine that you have the following character array, 
which represents the dimensions of some shipping boxes:
box_dimensions =
box1 1 3 5
box2 2 4 6
box3 6 7 3
box4 1 4 3
You need to find the volumes of the boxes to use in a calculation to 
determine how many packing "peanuts" to order for your shipping department.
Since the array is a 4 � 12 character array, the character representation of
the numeric information is stored in columns 6 to 12. 
Use the str2num function to convert the information into a numeric array, 
and usethe data to calculate the volume of each box. (
You'll need to enter the box_dimensions array as string data, 
using the char function.)

%}
box_dimensions = char('box1 1 3 5','box2 2 4 6', 'box3 6 7 3','box4 1 4 3');
%fail anlamad�m soruyu kafama g�re yap�yom
%XXXX^^^^
% calculator of each box_dimensions
for i=1:4
    peanuts(i) = str2num(box_dimensions(i,6)) * str2num(box_dimensions(i,8)) * str2num(box_dimensions(i,10))
end

title = box_dimensions(:,1:4);
data =  num2str(peanuts');
a = [' ';' ';' ';' '];
table = [title a data]

% length(box_dimensions)
% size(box_dimensions





%% 6.
clear all
clc

%{
Create a cell array called sample_cell to store the following individual arrays:
A =[1 3 5;3 9 2;11 8 2] (a double-precision floating-point array)
B =[fred ralph;ken susan](a padded character array)
C =4;6;3;1] (an int8 integer array)
a. Extract array A from sample_cell.
b. Extract the information in array C , row 3, from sample_cell.
c. Extract the name fred from sample_cell. 
Remember that the name fred is a 1 � 4 array, not a single entity.
%}

A = [1 3 5;3 9 2;11 8 2]
B = char('fred ralph','ken susan')
C_dOUBLE = [4;6;3;1];
C = int8(C_dOUBLE)
% class(C(1,1))
my_cell = {A,B,C}
celldisp(my_cell)

%a
sample_cell = my_cell{1} %caution > index in curly brackets
%b
C(3) = sample_cell(3,3) %?? any info pull from sample_cell ??
%c
data = my_cell{2}(1,1:4)


%% 7.
clear all
clc

%{
Store the information presented in the following table in a structure array.
Use your structure array to determine the element with the maximum density.
 
Metal    |Symbol |AtomicNumber |AtomicWeight |Density(g/cm3)|CrystalStructure|
Aluminum    Al         13          26.98       2.71            FCC
Copper      Cu         29          63.55       8.94            FCC
Iron        Fe         26          55.85       7.87            BCC
Molybdenum  Mo         42          95.94       10.22           BCC
Cobalt      Co         27          58.93       8.9             HCP
Vanadium    V|         23          50.94       6.0             BCC
%}


data.Metal = char('Aliminum','Copper', 'Iron' ,'Molybdenum','Cobalt', 'Vanadium');
data.Symbol = char('Al', 'Cu', 'Fe' , 'Mo' , 'Co' , 'V');
data.AtomicNumber = [13 29 26 42 27 23]';
data.AtomicWeight = [6.98 63.55 55.85 95.94 58.93 50.94]';
data.Density = [2.71 8.94 7.87 10.22 8.9 6.0]';
data.CrystalStructure = char('FCC','FCC','BCC' ,'BCC','HCP','BCC');
bosluk = data.Symbol*0; %gereksiz
table = [data.Symbol bosluk num2str(data.Density)]
% class(data.Density)



