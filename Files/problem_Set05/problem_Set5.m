%% problem_Set #5 
% Problem Solving
%
% USER DEFINED FUNCTIONS from week8.notes
%
% url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-*
% 
% Created on Tue Mar 25 14:02:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%if include input funciton. publish html outputs doesn't work!
%% notes from week8(lucun)

%{ 
1.User-defined functions are created in m-files. 

2.Before this function can be used, it must be saved into the current
folder. The file name must be the same as the function name in order for
MATLAB to find it. For example;
week8/ebob.m
week8/ekok.m
week8/max_value.m 
and we'll execute the func.s in the same folder week8/problem_Set5.m

3. You cannot execute a function m-file directly from the m-file itself

4. An example function m-file (my_func.m);

    function output = my_func(parameter1,parameter2,..)
    %help text for function
    output = parameter1+parameter2 (output is a output, not a keyword)
    end

5. Euclidian Algorithm = ekok(a,b) = a*b/ebob(a,b)


%}

%% 1.

clear all
clc

%{
Find the greatest common divisors of two given positive integers?
write func.(?) >> ebob(n1,n2)
%}

%{ 
gcd(50,20) = 10

50 20 | 2*
25 10 | 2
25 5  | 5*
5  1  | 5
1  1  |   

5*2 = 10

function algorithm:(in ebob.m)
%{
if n1==n2
    ebobu = n1;  
else
    ebobu=1;
    i=1;
    while i<n1 && i<n2  
        disp('d�n')
        if mod(n1,i)==0 && mod(n2,i)==0
            ebobu=i;
%             disp('�art')
        end
        i=i+1;
                
    end    
end
%}
%}
% n1=input('enter number 1:');
n1=50;
% n2=input('enter number 2:');
n2=20;
ebobu =ebob(n1,n2);
fprintf('greatest common divisior of %d and %d => %d\n',n1,n2,ebobu);


%ebob(50,20) = gcd(50,20) = 10 ?

%% 2.
clear all
clc
%{
Find the least common multiples of two given positive integers? 
>>ekok(n1,n2)

lcm(50,20) = 100

50 20 | 2*
25 10 | 2*
25 5  | 5*
5  1  | 5*
1  1  |

2 * 2 * 5 * 5 = 100

if we use Euclidian algorithm, this function very simple =
ekok = n1*n2/ebob(n1,n2)

%}

% n1=input('enter number 1:');
n1=50;
% n2=input('enter number 2:');
n2=20;
ekoku = ekok(n1,n2);
fprintf('least common multiple of %d and %d => %d\n',n1,n2,ekoku);

%ekok(50,20) = lcm(50,20) = 100 ?

%% 3.
clear all
clc

%{
 Find the maximum value and its index in a given vector?
max_value(n) function
%}

% a=input('enter the vector: ');
a=[1 2 3 4 55 6 7 8 9];
[max,index] = max_value(a);  
%note: if function's output more than one, we use vectors to assign values.


fprintf(' %d. element => %d is max value of this vector..\n',index,max)

% max_value(a) = max(a) = 55  ?


















