function ekoku = ekok(n1,n2)
%for show code enter 'open ekok.m' to command window!
%using euclidian algorithm:
% ekok(a,b) = a*b/ebob(a,b)
ekoku = n1*n2;
ekoku = n1*n2/ebob(n1,n2);
% fprintf('least common multiple of %d and %d => %d\n',n1,n2,ekoku);
end