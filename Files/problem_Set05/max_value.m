function [max,index] = max_value(n)
%first element max value, 
%second element it's index
max=0;
for i=1:length(n)
    if n(i)>max
        max = n(i);
        index = i;
    end
end
end




%algorithm
%{
given=
a = [1 2 3 4 5 23 6 7 8 9 10];

requested=
max_value(a) = 23 6
________________________



clear all
clc
a= [1 2 3 4 5 23 6 7 8 9 10];

max=0;
for i=1:length(a)
    if a(i)>max
        max = a(i);
        index = i;
       
    else
%         disp('1')
    end
end

 
%}













