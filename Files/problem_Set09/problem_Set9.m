%% problem_Set #9
%
% Reading & Writing Data from Files
%
%url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Thu Jun 11 16:25:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%if include input funciton. publish html outputs doesn't work!

%% 1.
clear all
clc
data.Title = char('Metal','Symbol','AtomicNumber','AtomicWeight','Density','CrystalStructure');
data.Title = string(data.Title) %if don't convert char to string all character assign as column..
data.Metal = char('Aliminum','Copper', 'Iron' ,'Molybdenum','Cobalt', 'Vanadium');
data.Metal = string(data.Metal);
data.Symbol = char('Al', 'Cu', 'Fe' , 'Mo' , 'Co' , 'V');
data.Symbol = string(data.Symbol);
data.AtomicNumber = [13 29 26 42 27 23]';
data.AtomicWeight = [26.98 63.55 55.85 95.94 58.93 50.94]';
data.Density = [2.71 8.94 7.87 10.22 8.9 6.0]';
data.CrystalStructure = char('FCC','FCC','BCC' ,'BCC','HCP','BCC');
data.CrystalStructure = string(data.CrystalStructure);

% data = struct2cell(data)
% xlswrite('prop_metal.xlsx',data)
% or 

data_new = [ data.Title'; data.Metal data.Symbol data.AtomicNumber data.AtomicWeight data.Density data.CrystalStructure]
xlswrite('prop_metal.xls',data_new)
data_new

%% 2.
clear all
clc

syms a b d x

se1 = x^3-(3*(x^2))+x
se2 = sin(x) + tan(x)
se3 = (2*x^2-3*x-2)/(x^2-5*x)
se4 = (x^2-9)/(x+3)

%% 3.

q3_A = se1/se2
q3_B = se3*se4
q3_C = se1+se3

%% 4.

sq1 = str2sym('x^2+y^2=4')
sq2 = str2sym('5*x^5-4*x^4 + 3*x^3 + 2*x^2 -x = 24')
sq3 = str2sym('sin(a) + cos(b) -x*c = d')
sq4 = str2sym('(x^3-3*x)/(3-x)=14')

%% 5.

[numerator_se4 denominator_se4] = numden(se4)
% [num_sq4 den_sq4] = numden(sq4) %doesn't work on equations.

%% 6.

expressions_expand = expand(se1)
expressions_factor = factor(se2)
expressions_collect = collect(se3)
expressions_simplify =simplify(se4)
% simple(se1) %undefinied function

equations_expand = expand(sq1)
% equations_factor = factor(sq2) %no factorization in equations 
equations_collect = collect(sq3)
equations_simplify =simplify(sq4)
% simple(sq1) %undefinied function

%% 7.

result_of_se1 = solve(se1)
result_of_se2 = solve(se1)
result_of_se3 = solve(se1)
result_of_se4 = solve(se1)

result_of_sq1 = solve(sq1)
result_of_sq2 = solve(sq2)
result_of_sq3 = solve(sq3)
result_of_sq4 = solve(sq4)

%% 8.
clear all
clc

%{

2*pi*f = sqrt((m*g*L)/I)

Where f , m, g, L, and I represent frequency, mass of the pendulum, 
acceleration due to gravity, distance from the pivot point to the 
enter of gravity of the pendulum, and inertia respectively. 
Use MATLAB's symbolic capability to solve for the length L.
%}

syms f m g L I;
% frequency of oscillation=
eq = 2*pi*f == sqrt((m*g*L)/I)
l_eq = solve(eq,L)



%% 9.

% m = 10
% f = 0.2
% I = 60
% g = 9.8 
length = subs(l_eq,{I,f,g,m},{60,0.2,9.8,10})
double(length)

%% 10.
clear all
clc
syms KE m V
eq = KE == 0.5 * m * V^2
solve(eq,V)
%% 11.
clear all
clc
syms t
h = ((-0.12)*(t^4)) + (12*(t^3)) - (380*(t^2)) + (4100*t) + 220

%% 11a-b-c
v= diff(h)

%b
a = diff(v)

%c (find t when h = 0)
t_ground = solve(h,0) ;
result = double(t_ground)
t_ground = result(2)

%% 11d (plot h,V,a graphs)

time =[0:t_ground];

ezplot(h,time); grid on;
title('Baloon Height');
xlabel('time, h');
ylabel('Baloon Height');
figure;
% hold on; %use if you want combine lines in one graph

ezplot(v,time); grid on;
title('Baloon Velocity');
xlabel('time, h');
ylabel('Velocity');
figure;
% hold on;

ezplot(a,time); grid on;
title('Baloon Acceleration');
xlabel('time, h');
ylabel('Acceleration');
% figure;
% hold on;

%% 11e (find h-max when v=0)

v_Zero = solve(v,0);
v_Zero = double(v_Zero)
%{
there are 3 moments which v=0  ; if look graphs, 3rd moments height max.  
%}
max_height_time = v_Zero(3)

h_max = subs(h,'t',max_height_time);

fprintf('max height = %f\n',h_max)








