%% week_2_notes 
% 
% BUILT-IN MATHEMATICAL FUNCTIONS in MATLAB
%
% url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Mon Feb 24 23:02:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-

%% Elementary Math Functions
clc

A = [1,2,3], B = [1,2,3;4,5,6;7,8,9]
disp("sum() function: ")
sum(A)
sum(B) %sum of array elements
disp("prod() function: ")
prod(A)
prod(B) %product of array elements
disp("max() min() function: ")
max(A),min(A)
max(B),min(B) %largest & smallest elemants in array
disp("mean() function: ")
mean(A)
mean(B) %avarage or mean value of array

%% Elementary Math Functions-2
clc

disp("rand(m,n) function: ")
rand(3,2) %return m-by-n random matris on the open interval (0,1)

disp("round() function: ")
v1= [13.499 13.500 13.501 -23.499 -23.500 -23.501]
round(v1) %round to nearest integer

disp("floor() function: ")
v2= [1.2 2.99 3.5 4 -3.1 -6.99  -0.1923]
floor(v2) %round toward negative infinite

disp("ceil() function: ")
v2= [1.2 2.99 3.5 4 -3.1 -6.99  -0.1923]
ceil(v2) %round toward positive infinite

disp("fix() function: ")
v2= [1.2 2.99 3.5 4 -3.1 -6.99  -0.1923]
fix(v2) %round toward zero

disp("mod(x,y) function: ")
mod(3,2) %=1
mod(3,-2) %=-1 modulus after division

disp("rem(x,y) function: ")
rem(3,2) %=1
rem(3,-2) %=1 remainder after division

%% Elementary Math Functions-3
clc

disp("factorial() function: ")
factorial(5) %=120 take factorial

disp("perms() function: ")
perms([1,2,3]) %all possible permutations(in row)

disp("factor() function: ")
factor(60) %= 2 2 3 5 prime factors

disp("primes() function: ")
primes(12) %list prime numbers until to 12

disp("isprime() function: ")
isprime([1907,1905,1903]) %array elements thar are prime numbers

disp("lcm(n1,n2) function: ")
lcm(30,20) %=60  ekok - least common multiple

disp("gcd(n1,n2) function: ")
gcd(30,20) %=10  obeb- greatest common divisor 

%% Complex Number Functions
clear all
clc

x = 3 + 5i           %= 3+5i
y = 2 + 4j           %= 2 + 4i
z = 1 + 2i + 4j      %= 1 + 6i

disp("complex() function: ")
%complex(a,b) = a +bi
complex(3,5)

disp("abs() function: ")
abs(8+6i) %Absolute value and complex magnitude

disp("angle() function: ")
angle(3+4i) %phase angle of the complex number

disp("conj() function: ")
conj(6+8i) %complex conjugate

%% Complex Number Functions-2
clc
format rat

disp("real() function: ")
real(3+5i) %= 3 real part of complex number

disp("imag() function: ")
imag(3+5i) %=5 imaginary part of complex number

disp("isreal() function: ")
x = 3+5i
disp("isreal(x): "+isreal(x))
disp("isreal(x+conj(x)): "+isreal(x+conj(x)))

disp("sign(x) function: ")
sign(4); %=1
sign(-4); %=-1
sign(0); %=0
%if x is complex number sign(x) = x./abs(x)
x2 = 3+4i;
sign(x2)


%% Trigonometric Functions
clear all
clc
format short

disp("sin(pi/2) : "+ sin(pi/2))
disp("cos(2*pi) : "+ cos(2*pi))
disp("tan(pi/4) : "+ tan(pi/4))
disp("cot(pi/4) : "+ cot(pi/4))
disp("_____________")
disp("sind(90)  : "+ sind(90)) %add d to end of sin,cos.. functions , input in degrees
disp("_____________")
disp("sinh(1)  : "+ sinh(1)) %add h to end of sin,cos.. functions , input in hyperbolic funcs.
disp("_____________")
disp("acos(1)  : "+ acos(1)) %add a to beginning of sin,cos.. functions , convert to inverse of trigo funcs.
disp("acosd*(0)  : "+ acosd(0)+" degree*")

%% Exponential Functions 
clear all
clc
format short

disp("Square root : ")
disp("sqrt(25): "+sqrt(25))
fprintf("\n")

disp("Exponential : ")
disp("exp(1)  : "+exp(1))
fprintf("\n")

disp("expm1() function: ")
disp("expm1(1) : "+expm1(1)) %compute exp(x)-1
fprintf("\n")

disp("Natural Logarithm: ")
disp("log(exp(3)) : "+log(exp(3)))
disp("log10(100) : "+log10(100))
disp("log2(16)) : "+log2(16)+"     *speacial func. for log base 2&10 ")
fprintf("\n")

disp("log1p(x) function: ")
log1p(1) %compute log(1+x)

disp("reallog() function: ")
a = exp(2);
reallog([a,a^2]) %natural logarith for nonnegative real arrays

%% Polynomial Functions
clear all
clc

p1 = [1,7,12] ,p2 = [2,3,4]
disp("roots() function: ")
roots(p1) %polynomial roots

disp("polyval() function: !!!: ")
polyval(p1,[3,5]) %polynomial evaluation
%I Don't Understand look later!!

disp("conv() function !!!: ")
conv(p1,p2) %convolution and polynomial multiplication
%I Don't Understand look later!!

disp("poly() function !!!: ")
a = [1:3]
poly(a) %generate poly from matrix ??

disp("polyder() function !!!: ")
polyder(p1) %polynomial derivative

disp("polyint(p,k) function !!!: ")
polyint(p2,0) %idk anything

%% Matrix Operation Functions
clear all
clc

disp("zeros(m,n) function : ")
zeros(3,2) %generate m-by-n matrix of zeros

disp("ones(m,n) function : ")
ones(2,3) %generate m-by-n matrix of ones

disp("eye(n) function : ")
eye(3) %generate nxn identity matrix

disp("diag(x) function : ")
v = 1:3; A = [1 2 3;4 5 6;7 8 10]
diag(A) %returns elements on diagon
diag(v)

disp("det() function : ")
det(A) %matrix determinant

%% Matrix Operation Functions-2
clc

disp("inv() function : ")
inv(A) %matrix inverse

disp("norm() function :  !idk!")
norm(v,1) %vector and matrix norms !idk

disp("null() function : ")
A2 = null(A) %3x0 empty matrix %null space

disp("rank() function : ")
rank(A) %rank of matrix

disp("rref() function : ")
rref(A) %reduced row echelon form

disp("trace() function : ")
trace(A) %sum of diagonal elemants = trace of matrix

disp("sqrtm() function : ")
A3 = sqrtm(A); %matrix square root
A3 * A3
% disp((A3 * A3) == A) %nteresting

disp("eig() function : ")
disp("V = Eigen vectors, E = Eigen Values: ")
[V,E] = eig(A)

disp("expm() function : ")
expm(A) %matrix exponential

disp("linsolve() function : ")
%solve the linear system A*x=B

A = [3 5 1 ; 1 1 0 ; 0 3 5];
B = [16 ; 3 ; 21];
linsolve(A,B)


%% Output Options 
clc
%{ 
Other special format commands used for �fprintf� are listed in the table below

Format |    Description

%d     |     Base 10 values
%e     |     Exponential notation (3.141593e+00)
%f     |     Fixed-point notation
%g     |     The more compact of %e or %f, with no trailing zeros
%E     |     Same as %e, but uppercase (3.141593E+00)
%G     |     The more compact of %E or %f, with no trailing zeros
%c     |     Single character
%s     |     String of characters
%o     |     Base 8 (octal)
%u     |     Base 10
%x     |     Base 16 (hexadecimal), lowercase letters
%X     |     Same as %x, uppercase letters

%}

















































