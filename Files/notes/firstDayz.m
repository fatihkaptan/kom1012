% -*- coding: utf-8 -*-
% 
% Created on Sun Feb 9 01:15:08 2020
% 
% @author: Kaptan

%% matrislerde islemler (matris tanimlari bu bolumde)
clear all 
clc 

a = [1 2 -3 ; 4 0 -2] % 2x3
a2= [5 10 15 ; 2 4 6] % 2x3
b = [3 1 ; 2 4 ; -1 5] % 3x2


%% skaler islemler
disp("skaler toplam: ")
disp(a+a2)
disp("skaler cikarma: ")
disp(a2-a)
disp("skaler carpim: ")
disp(a.*a2)  %dikkat skaler operator = .*
disp("skaler bolum: ")
disp(a2./a) %dikkat skaler operator = ./
disp("sayi ile toplam: ")
disp(a+2)
disp("sayi ile carpim: ")%operator = * inaf
disp(a*2)
disp("Nokta carpim islemi ornek: ") 
disp(12*a - 2*a2)

%% vektor komutlari 
clear all
clc

x= 1:3:20 %vektor olusturma (start,artis,stop)

%linspace komutu ile esit aralikli sayilar olusturulur

y= linspace(1,100,10) %perimeters(start,stop,n) n=how many spaced points

%logspace komutu ile logaritmik aralikli sayilar olusturulur.

z = logspace(1,5,3) %perimeter(start10^a,stop10^b,n) n= logarithmically spaced points

%vektor elemanlari toplama ve ortalamasini alma: 

v1= [1 2 3 4 5 6 7 8 9 0]
v2= [2;4;6;8;10;12;14;16;18;20]

toplam1 = sum(v1)
toplam2 = sum(v2)
ortalama = mean(v1)

%vektor uzunlugu(eleman sayisi) bulma: length()
uzunluk = length(v1)

%min/max elemanlar:

sonuc2 = max(v1) + min(v2)

%elemanlarin birbiriyle carpimi: prod()

carpim = prod(v1)

%matris elemanlarinin isaret tayini: sign()

m1 = [2 4 6;-1 -2 -3]

isaret = sign(m1) %ciktilar pozitifler icin +1, negatifler icin -1

% sifira dogru yuvarlama fonskiyonu: fix() 
clc 

v3= [1.2 2.99 3.5 4 -3.1 -6.99 -9.5 0 -0.1923]


yuvarla= fix(v3) %sayi sifira giderkenki ilk tamsayi ciktisini verir

% eksi ve arti sonsuza dogru yuvarlama fonsksiyonlari: floor(-),ceil(+)

eksi= floor(v3)
arti= ceil(v3)

%en yakin sayiya yuvarlama: round()
v4= [13.499 13.500 13.501 -23.499 -23.500 -23.501]

enYakin= round(v4) %bucuktan sonrasi kendi sonsuzuna dogru gider...

%vektor elemanlarini siralama: sort()

sirala = sort(v3)
tersine= sort(v3,'descend')  %ctrl+F1 ile parametrelere bak! 















