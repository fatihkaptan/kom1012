%% week1_notes
%
% Created on Sat Feb 15 21:50:08 2020
% 
% @author: Kaptan

%% Relational Operators

%{
sign|func eq| desc

==  | eq    | equal to
~=  | ne    | not equal to
>   | gt    | greater than
>=  | ge    | greater than or equal to
<   | lt    | less than
<=  | le    | less than or equal to

%}

% let's define matrices
clear all 
clc

a= [ 1 2 3 ; 11 12 13];
b= [ 4 2 2 ; 7 15 13 ];

% operations


a
b
disp("a==b:")
disp(eq(a,b)) %or a==b
disp("a~=b:")
disp(ne(a,b)) %or a~=b
disp("a<b:") 
disp(a<b)   %or lt(a,b)
disp("a>b:") 
disp(a>b)  %or gt(a,b)
disp("a>=b: ")
disp(ge(a,b)) %or a>=b
disp("a<=b:")
disp(le(a,b)) %or a<=b

c1 = a<b;
disp(c1)

%% Logical Operators
clear all
clc

%{
sign | operator | func

a&b  | and      | and(a,b) 
a|b  | or       | or(a,b)
xor  | xor      | xor(a,b)
~a   | not      | not(a)   !

%}

% let's define matrices

m = [ 1 2 3 ; 0 2 4 ; 5 10 0]
n = [ 0 1 2 ; 3 6 0 ; 0 0 0]

% operations

disp("m & n:")
disp (and(m,n)) %or a&b
disp("m | n:")
disp (m|n) % or or(m,n)
disp("xor(m,n):")
disp (xor(m,n))
disp("not(m):")
disp(~m) %or not(m)
disp("not(n):")
disp(~n) %or not(n)


%% Arithmetic Operators (for scalars)

%{
op.             | in matlab

addition        | a+b
substraction    | a-b
multiplication  | a*b
r. division     | a/b
l. division     | a\b
power           | a^b
%}

%% Special Cases and Functions
clear all
clc

% && and || operators generate result whatever the value of second variable
a= 1; %logically true
b= 0; %logically false

fprintf("a(True)  || (any_variable): %d\n",a||g)
fprintf("b(False) && (any_variable): %d\n",b&&h)

%{
 'all' and 'any' functions:    (nonzero)

they're defined also for vectors and run same as and&,or| ;
if all elements of vector are nonzero >> all(vector) = 1
if any element of vector is a nonzero >> any(vector) = 1
%}

v1 = 3:9
v2 = -3:3
v3 = [ 0 0 0 0 0 0 0]


fprintf("all(v1): %d\n",all(v1))
fprintf("any(v1): %d\n",any(v1))
fprintf("all(v2): %d\n",all(v2))
fprintf("any(v2): %d\n",any(v2))
fprintf("\nany(v3): %d\n",any(v3))

%% Logical Functions
clear all
clc

%{
func         | result

ischar(a)    | if <<a>> is a character array(string) = logically 1
isnumeric(a) | if <<a>> is numeric array = logically 1
isempty(a)   | if <<a>> is an empty array = logically 1
isinf(a)     | if <<a>> is an infinite element = logically 1
isnan(a)     | if <<a>> is not a number = logically 1
%}

disp("char func.  result: "+ischar('kaptan'))
disp("char func. result: "+ischar("kaptan")) %interesting

disp("numeric func. result: "+isnumeric(92))
disp("numeric func. result: "+isnumeric('092'))

disp("empty func. result: "+isempty(''))
disp("empty func. result: "+isempty(0))

disp("infinite func. result: "+isinf(1/0))
disp("infinite func. result: "+isinf(4))

disp("non-number func. result: "+isnan(0/0))
disp("non-number func. result: "+isnan(5))

%% using Colon Operator:
clear all
clc

%vectors can also be generated with the colon operator
% var = start:stop  or var = start:inc/desc:stop
%examples:

vector1 = 1:10    %from 1 to 10 increment coefficent default=1
vector2 = 0:2:10  %from 0 to 10 increment coefficent set=2
vector3 = 0:0.5:4 %from 0 to 4 increment coefficent set=0.5
vector4 = 10:-2:0 %from 10 to 0 descrement coefficent set = 2

%by the way <'> operator is transposing vector..>>>
vector5 = vector4';
disp("transpose of vector4: ")
disp(vector5)

%meanwhile we can refer to particular elements of a vector by means of subscripts.
%example:

first_element           = vector5(1) %gives first element of vector
last_element            = vector5(6)
first_three_elements        = vector5(1:3)
second_and_fourth_elements  = vector5([2,4])




%% linspace() and logspace() functions:
clear all
clc

%the linsapce func. generates linearly spaced vectors like colon operator but it can set number of points
%linspace (start,stop,number of points)

space1 = linspace(1,2); %default number of points = 100 
space2 = linspace(10,100,5) %divides into 4 equal parts and 5 points..
space3 = linspace(1,10,10) % 9 equal space, 10 points...

%the logspace func. generates logarithmically spaced vectors.
%generates n points between decades 10^a and 10^b
%logspace(start:stop,n)

log_s1 = logspace(1,5,5) % from 10^1 to 10^5, divide 5 equal logarithmic space
log_s1 = logspace(1,5,3) %from 10^1 to 10^5, divide 3 euqal logarthmic space...


%% MATRICES 
clear all
clc

%a matrix can be constructed from column vectors of the same lenght:

x = 0:30:180;
table = [ x' sin(x*pi/180)' ]

%subscripts using similar to the vectors ; matrix(row,column)
example_4x2 = table(4,2)

%% Matrix built-in funcs
clear all
clc

%zeros() : return matrix of zeros
disp("-----zeros func.-----")
sifir1 = zeros(3)  % generate square n-by-n matrix
sifir6 = zeros(4,6) % generate n-by-m matrix

disp("-----ones func.------")
%ones() : return matrix of ones

birdir1 = ones(3)
birdir0 = ones (3,4)
disp("-----eyes func.------")
%eye() : return identity matrix

kosegen1 = eye(3)
kosegen2 = eye (3,4)

disp("-----size and lenght func.------")

%size() : gives the size of each dimension of matrix
boyut = size(kosegen2)

%lenght() : gives the largest dimension of matrix
uzunluk = length(kosegen2)

%% Arithmetic Operators (for arrays)
clear all
clc

%{
 
%{ 
ELEMENT BY ELEMENT
    operation     | operator | desc. 

    addition      | a+b      | element by element matrix addition
    substraction  | a-b      | element by element matrix substraction
    multiplication| a.*b     | element by element matrix multiplication (a or b can be scalar)
    r. division   | a./b     | element by element right division ( a or b can be scalar)
    l. division   | a.\b     | element by element left division ( a or b can be scalar)
    power         | a.^b     | element by element power ( a or b can be scalar)
%}
%{
MATRIX OPERATION : 
operation             | operator | desc. 

matrix multiplication | a*b      | matrix multiplication
r. matrix division    | a/b      | the inverse of the matrix b is multiplied by the matrix a from left
l. matrix division    | a\b      | the inverse of the matrix a is multiplied by the matrix b from left
%}
%}
 
%% submatrices
clear all
clc 
m1 = [1 2 3 ; 4 5 6 ; 7 8 9]
sub1 = m1(:,1) % gives first column
sub2 = m1(:,2) % gives second column
sub3 = m1(2,:) % gives second row
sub4 = m1(1:2,1:2) % 2x2 submatrix of m1 matrix
sub5 = m1(2:3,1:2) % (row interval,column interval)

%% constant and permanent variables in MATLAB
clear all
clc

3+5
disp("last answer: "+ans)
disp("pi number: "+pi)
n1 = 1/0
disp(date)
disp(realmax)
disp(realmin)
% disp(bitmax)






















