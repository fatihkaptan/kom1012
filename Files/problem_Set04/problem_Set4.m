%% problem_Set #4 
%
%url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Mon Mar 2 22:55:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%if include input funciton. publish html outputs doesn't work!

%% 1.
clear all
clc

%{
1. Prompt the user to enter a vector.
i. Use a for loop to sum the elements in the entered vector.
ii. Use a while loop to sum the elements in the entered vector.
iii. Check the answers with the sum function.
%}

% v = input("enter a vector in square brackets[..] : ");
v= 5:10
total = 0;
for i=1:length(v)
    total = total + v(i);
end

disp("total sum with sum: "+total)
total2=0;  
j=0;  
while j<length(v)  
    j = j+1;
    total2 = total2 + v(j);
    
end


disp("total for while : "+total2)

sum(v) == total & sum(v) ==total2


%% 2.
clear all
clc

%{
A Fibonacci sequence is composed of elements created by adding the 
two previous elements. 
The simplest Fibonacci sequence starts with 1, 1 and proceeds as follows:
1, 1, 2, 3, 5, 8, 13, ...
However, a Fibonacci sequence can be created with any two starting numbers. 
Fibonacci sequences appear regularly in nature. For example, 
the shell of the chambered nautilus cgrows in accordance with a Fibonacci 
sequence.
Prompt the user to enter the rst two numbers in a Fibonacci sequence and the total
number of elements requested for the sequence. Find the sequence and store 
it in an array by using a for loop.


%}

% fibo(1) = input("enter the first number: ");
fibo(1) =1;
% fibo(2) = input("enter the second number: ");
fibo(2) =1;
% kac_tane = input("how many elements in Fibo sequence? >> ");
kac_tane = 11;

for i=3:kac_tane
    fibo(i)= fibo(i-1) + fibo(i-2);
end
    
fibo


%% 3.
clear all
clc

%{
do fibonacci with while loop..

%}

% fibo(1) = input("enter the first number: ");
fibo(1) =1;
% fibo(2) = input("enter the second number: ");
fibo(2) =1;
% kac_tane = input("how many elements in Fibo sequence? >> ");
kac_tane = 11;
i=3;
while i<kac_tane+1
    fibo(i) = fibo(i-1) + fibo(i-2);
    i=i+1;
end
fibo
    
%% 4.
clear all
clc

%{
One interesting property of a Fibonacci sequence is that the ratio of the 
values of adjacent members of the sequence approaches a number called 
"the golden ratio". Create a program that accepts the first two numbers of
 a Fibonacci sequence as user input and then calculates additional values 
in the sequence until the ratio of adjacent values onverges to within 0.001.
You can do this in a while loop by comparing the ratio of element k to element k ? 1 and
the ratio of element k ? 1 to element k ? 2. If you call your sequence x, 
then the code for the while statement is
while abs(x(k)/x(k ? 1) ? x(k ? 1)/x(k ? 2)) > 0.001
%}


% fibo(1) = input("enter the first number: ");
fibo(1) = 1;
% fibo(2) = input("enter the second number: ");
fibo(2) = 1;
% kac_tane = input("how many elements in Fibo sequence? >> ");
kac_tane = 11;
i=3;
while i<kac_tane+1
    fibo(i) = fibo(i-1) + fibo(i-2);
    while abs(fibo(i)/fibo(i - 1) - fibo(i - 1)/fibo(i - 2)) > 0.001
        fprintf("ratio of %d/%d = %f\n",fibo(i-1),fibo(i-2),fibo(i-1)/fibo(i-2))
        fprintf("difference of rates = %f\n\n",abs(fibo(i)/fibo(i - 1) - fibo(i - 1)/fibo(i - 2)))
        fibo
        disp("_______________________________")
        break
    end
    i=i+1;
end
fibo

fprintf("golden ratio of last elements>> %d/%d = %f\n ",fibo(i-1),fibo(i-2),fibo(i-1)/fibo(i-2))
son_altin_oran = fibo(i-1)/fibo(i-2);

% while abs(fibo(i)/x(i - 1) - x(i - 1)/x(i - 2)) > 0.001
    
%% 5.
clear all
clc

%{
Edmond Halley (the astronomer famous for discovering Halley's comet) 
invented a fast algorithm ?for computing the square root of a number, 
A. Halley's algorithm approximates
sqrt(A) as follows:
Start with an initial guess x1 . The new approximation is then given by
y(n) = (1/A)*(x(n))^2
x(n+1) = (x(n) /8)*(15-y(n)(10-3*y(n)))

These two calculations are repeated until some convergence criterion, ?, is met.
|x(n+1) ? x(n)| <= ?
Write an m-file that approximates the square root of a number. 
It should prompt the user to enter two inputs, the initial guess and the 
convergence criterion. Test your code by comparing it to the value calculated 
with the built-in MATLAB function, sqrt.

E = convergence criterion
G = initial guess
A = the number taking sqrt
%}

% A = input('enter the number that take sqrt');
A = 225;

% x(1) = input('enter the initial guess: ');
x(1) = 1;

% E = input('enter the converge criterion: ');
E = 0.001;

n=1;
y(n) = (1/A).*((x(n)).^2); %these operations are for calculate x(2)
x(n+1) = (x(n) ./8).*(15-y(n).*(10-(3.*y(n))));

while abs(x(n+1)-x(n)) > E
    n=n+1; %for calculate next x(n) and loop
    y(n) = (1/A).*((x(n)).^2); 
    x(n+1) = (x(n) ./8).*(15-y(n).*(10-(3.*y(n))));
    
end
x
kac_adim = n
% y



    
    
    
    
    
    
    
    
    