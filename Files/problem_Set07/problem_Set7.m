%% problem_Set #7 
%
% 2D Plotting
% 
% url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Tue Apr 8 14:02:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%if include input funciton. publish html outputs doesn't work!
%% 1.
clear all
clc

%{
1. Create plots of the following functions from x = 0 to 10.
i. y = e^x
ii. y = sin(x)
iii. y = 5x^2+ 2x + 4
iv. y =sqrt(x)
Each of your plots should include a title, an x -axis label, a y -axis label, and a grid.
%}

%i:

x=1:10;
y=exp(1).^x;
plot(x,y);
title('i. y=e^x graph');
xlabel('x');
ylabel('y = e^x');
grid;
figure;

%ii

y=sin(x);
plot(x,y);
title('ii. y=sin(x) graph');
xlabel('x' );
ylabel('y = sin(x)');
grid;
figure;

%iii

y=5.*x.^2 +2.*x + 4;
plot(x,y);
title('iii. y=5x^2 +2x+4 graph');
xlabel('x' );
ylabel('y=5x^2 +2x+4');
grid;
figure;

%iv

y=sqrt(x);
plot(x,y);
title('iv. y=sqrt(x) graph');
xlabel('x');
ylabel('y=sqrt(x)');
grid;

%% 2.
clear all
clc

%{
Plot the following functions on the same graph for x values from ?? to ?, selecting spacing
to create a smooth plot:
i. y = sin(x) (red and dashed)
ii. y = sin(2x) (blue and solid)
iii. y = sin(3x) (green and dotted).
Do not include markers on any of the graphs. In general, markers are included only on
plots of measured data, not for calculated values.
Adjust the plot so that the x -axis goes from ?6 to +6, add a legend and add a text box
describing the plots.
%}

x=-pi:pi/50:pi;
y=sin(x);
plot(x,y,'--r')
hold on;
y=sin(2.*x);
plot(x,y,'b')
hold on;
y=sin(3.*x);
plot(x,y,'g:')
legend('y=sin(x)', 'y=sin(2x)', 'y=sin(3x)')
axis([-6,6,-1,1])
text(3.5,0.6,'x = -pi to pi');


%% 3.
clear all
clc

%{
The distance a projectile travels when fired at an angle ? is a function of time and 
can be divided into horizontal and vertical distances according to the formulas

         horizontal(t) = t*V0* cos(?)
and
         vertical(t) = tV0 sin(?) ?1/2*gt^2

where;
horizontal = distance traveled in the x direction (m)
vertical = distance traveled in the y direction (m)
V0 = initial velocity(m/s)
g = acceleration due to gravity, 9.8m/s2
t = time, (s)

i. Suppose the projectile just described is red at an initial velocity of
100m/s and a launch angle of ?/4rad(45?). Find the distance travelled both horizontally and
vertically (in the x and y directions) for times from 0 to 20s with a spacing of 0.01 seconds.
a) Graph horizontal distance versus time.
b) In a new gure window, plot vertical distance versus time (with time on the x -axis).
Don't forget a title and labels.

ii. In a new figure window, plot horizontal distance on the x -axis and
vertical distance on the y -axis.

iii. Calculate three new vectors for each of the vertical (v1, v2, v3) and horizontal (h1, h2, h3)
distances traveled, assuming launch angles of ?/2, ?/4 and ?/6.
a) In a new figure window, graph horizontal distance on the x -axis and
vertical distance on the y -axis, for all three cases. (You'll have three lines.)
b) Make one line solid, one dashed, and one dotted. Add a legend to identify which line is which.

%}

g= 9.8;
v0 = 100;
teta = pi/4;
t = 0:0.01:20;

horizontal = t.*v0.*cos(teta);
vertical = (t.*v0.*sin(teta)) - ((g.*(t.^2))./2);

%i.a):

plot(t,horizontal);
title('Horizontal Distance x Time Graph');
xlabel('time' );
ylabel('Horizontal distance');
figure;


%i.b):

plot(t,vertical);
title('Vertical Distance x Time Graph');
xlabel('time' );
ylabel('Vertical distance');
figure;

%ii.

plot(horizontal,vertical);
title('Horizontal Distance x Vertical Distance');
xlabel('Horizontal Distance' );
ylabel('Vertical Distance');
figure;

%iii.

teta1 = pi/2;
teta2 = pi/4;
teta3 = pi/6;
%horizontal distances
h1 = t.*v0.*cos(teta1);
h2 = t.*v0.*cos(teta2);
h3 = t.*v0.*cos(teta3);
%vertical distance
v1 = (t.*v0.*sin(teta1)) - ((g.*(t.^2))./2);
v2 = (t.*v0.*sin(teta2)) - ((g.*(t.^2))./2);
v3 = (t.*v0.*sin(teta3)) - ((g.*(t.^2))./2);

plot(h1,v1,'g')
hold on;

plot(h2,v2,'b')
hold on;

plot(h3,v3,'r')
title('Horizontal D. x Vertical D.');
legend('Horizontal D. 1 x Vertical D. 1','Horizontal D. 2 x Vertical D. 2','Horizontal D. 3 x Vertical D. 3')



%% 4.
clear all
clc

%{
In Problem 1, you created four plots. Combine these into one gure with four subwindows,
using the subplot function.
%}

x=1:10;

y=exp(1).^x;
subplot(2,2,1); plot(x,y); grid on;
title('y=e^x graph');
xlabel('x');
ylabel('y = e^x');

y=sin(x);
subplot(2,2,2); plot(x,y); grid on;
title('ii. y=sin(x) graph');
xlabel('x');
ylabel('y = sin(x)');

y=5.*x.^2 +2.*x + 4;
subplot(2,2,3); plot(x,y); grid on;
title('iii. y=5x^2 +2x+4 graph');
xlabel('x');
ylabel('y=5x^2 +2x+4');

y=sqrt(x);
subplot(2,2,4); plot(x,y); grid on;
title('iv. y=sqrt(x) graph');
xlabel('x');
ylabel('y=sqrt(x)');


%% 5.
clear all
clc

%{
In Problem 3, you created four plots. Combine these into one figure with four subwindows,
using the subplot function.
%}
% re-create variables !!
g= 9.8;
v0 = 100;
teta = pi/4;
t = 0:0.01:20;
horizontal = t.*v0.*cos(teta);
vertical = (t.*v0.*sin(teta)) - ((g.*(t.^2))./2);
teta1 = pi/2;
teta2 = pi/4;
teta3 = pi/6;
h1 = t.*v0.*cos(teta1);
h2 = t.*v0.*cos(teta2);
h3 = t.*v0.*cos(teta3);
v1 = (t.*v0.*sin(teta1)) - ((g.*(t.^2))./2);
v2 = (t.*v0.*sin(teta2)) - ((g.*(t.^2))./2);
v3 = (t.*v0.*sin(teta3)) - ((g.*(t.^2))./2);
%finitto


subplot(2,2,1); plot(t,horizontal); grid on;
title('t,horizontal');

subplot(2,2,2); plot(t,vertical); grid on;
title('t,vertical');

subplot(2,2,3); plot(horizontal,vertical); grid on;
title('horiontal,vertical');

subplot(2,2,4); plot(h1,v1,h2,v2,h3,v3); grid on;
title('h x v 1,2,3');

