%% problem_Set #2-3 
%
%url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Mon Mar 2 22:55:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%if include input funciton. publish html outputs doesn't work!

%% 1.
clear all
clc
%{
Create an m-file that prompts the user to enter a value of x and then 
calculates the value of sin(x).

mkdir =  create folder
edit ....* = create file
%}

x = input("enter x: ");
fprintf("sin(%f) = ",x)
fprintf("%d\n",sin(x))

%% 2.
clear all
clc

%{
. Create an m-file that prompts the user to enter an array of numbers. Use
the length function to determine how many values were entered, and use the
disp function to report your resultsto the command window.
--use length() func and [](list)
%}

liste = [];
liste = input("pls enter the number list in the square bracket [ n1 n2 n3...] >> ")
disp("length of list = "+length(liste))

%% 3.
clear all
clc

%{ 
Create an m-file that prompts the user to enter an array of numbers. Use
the length function to determine how many values were entered, and use the
fprintf function to report your results to the command window.
%}

liste = [];
liste = input("pls enter the number list in the square bracket [ n1 n2 n3...] >> ")
fprintf("length of list = %d\n",length(liste))

%% 4.
clear all 
clc

%{
This problem requires you to generate temperature conversion tables. 
Use the following equation, which describe the relationships between 
temperatures in degrees Fahrenheit TF and degrees Celsius TC :
TF = (9/5)*TC + 32
%}

TC = -50:10:150;
TF = (9/5)*TC + 32;

tablo = ["celcius" TC ; "fahrenheit" TF]'

%% 5.
clear all
clc

%{
If a stone is thrown vertically upward with an initial speed v0 , 
its vertical displacement h 
after an elapsed time t is given by the formula
h = v0*t ? (1/2)*g*(t^2)

where g is the acceleration due to gravity (air resistance is ignored). 
Create an m-file for the following tasks.
i. Prompt the user to enter a positive value of v0 .
ii. Utilizing g = 9.8m/s^2 and assuming h(0) = 0, 
compute the maximum possible height that the stone can reach, 
and the time tf > 0 for which hf = 0.
iii. Obtain t1 which is the rounded value of 100�tf toward zero and compute tr = (t1)/100.
iv. Define a vector named tv consisting of the time passes during the travel of the stone
with the intervals of 0.01s. 
Note that the first element of tv will be zero and the last element of tv will be equal to tr .
v. Compute h(t) for each element in tv and assign them to the vector named hv.
vi. Define a matrix m whose first column is equal to tv and the second column is equal to hv.
vii. Finally, use the function plot(tv, hv) to plot the time-height graph.


%}


g = 9.8;
h0 = 0;
v0 = input("enter a initial velocity of stone >> ");
t = v0/g;
h_max = v0*t - (1/2)*g*(t^2) %max height
t_final = 2 * t %time for which hf = 0

%iii. toward zero round function = fix()
t1 = fix(100*t_final);
t_rounded = t1/100

%iv
tv = 0:0.01:t_rounded;
%v
hv = v0.*tv - (1/2)*g*(tv.^2); %caution dot operations
%vi
m = [tv ;hv]';
%vii
plot(tv,hv)

%% 6.
clear all
clc

%{
Create a program that first prompts the user to enter a value for x 
and then prompts the user to enter a value for y. 
If the value of x is greater than the value of y, 
send a message to the command window telling the user that x > y. 
If x is less than or equal to y, send a message to the command window
telling the user that y >= x.

%}

x = input ("enter x: ");
y = input ("enter y: ");

if x>y
    disp("x>y")
elseif x<=y
    disp("x<=y")
else
    disp("error")
end
        
%% 7.
clear all
clc

%{ 
The inverse sine (asin) and inverse cosine (acos ) functions are valid 
only for inputs between ?1 and +1, because both the sine and the cosine
 have values only between ?1 and +1.
MATLAB interprets the result of asin or acos for a value outside the range 
as a complex number. For example, we might have acos(?2)
ans =
3.1416 ? 1.3170i
which is a questionable mathematical result. Create an m-fle called 
my-asin that accepts a single value of x and checks to see if it is 
between ?1 and +1 (?1 <= x <= 1). If x is outside the range, 
send an error message to the screen. If it is inside the allowable range,
return the value of asin.

--error message

%}

x = input("enter x >> ");
if x>=-1 & x<=1
    disp("arcsin(x) = "+asin(x))
else
    disp("please enter value between -1 and 1")
end
    
%% 8.
clear all
clc
%{
Create a program that prompts the user to enter a scalar value for the 
outside air temperature. If the temperature is equal to or above 80?F , 
send a message to the command window telling the user to wear shorts. 
If the temperature is between 60?F and 80?F send a message to the command 
window telling the user that it is a beautiful day. 
If the temperature is equal to or below 60?F , send a message to the 
command window telling the user to wear a jacket or coat.

>=80  = wear shorts
60-80 = it is a beautiful day
<= 60 = wear a jacket or coat
%}

temp = input("please emter temperature in Fahrenheit >> ")
if temp >= 80
    disp("wear short:-)")
elseif temp >60 & temp<80
    disp("it is a beautiful day:)")
elseif temp <= 60
    disp("wear a jacket or coat!")
else
    disp("enter valid number")
end


%% 9.
clear all
clc
%{
At a local university, each engineering major requires a dierent number of 
credits for graduation. 
For example, recently the requirements were as follows:
1 Civil Engineering 130
2 Chemical Engineering 130
3 Computer Engineering 122
4 Electrical Engineering 126.5
5 Mechanical Engineering 129
Prompt the user to select an engineering program from a menu. 
Use a switch/case structure to send the minimum number of credits 
required for graduation back to the command window.

%}

fprintf("1> Civil Engineering \n2> Chemical Engineering \n3> Computer Engineering \n4> Electrical Engineering \n5> Mechanical Engineering \n")
ix = input("which program would you like to see? > ");
switch ix
    case 1
        disp("min credit : 130")
    case 2
        disp("min credit : 130")
    case 3
        disp("min credit : 122")
    case 4
        disp("min credit : 126.5")
    case 5
        disp("min credit : 129")
    otherwise
        disp("out of range!")
end





















