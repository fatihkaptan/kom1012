function total = faktoriyel()
while true
    x = input('enter x: ');
    if x==0
        total=1;
    elseif x>0
        total=1;
        for i=1:x
            total = total *i;
        end
    elseif x<0
        disp('breaking..')
        total=('error, negative number!');
        break
    else
        disp('error, only integer!')
    end
    fprintf('factorial = %d\n\n',total)
end
return
end