function result = my_cos(x)
total=0;
division=1;
i=0;
j=0;
while j<15 %15 step taylor series(optional)
    i=i+2;
    j=j+1;
%     a=((-1).^j); %line 9-10 about algorithm
%     fprintf('i = %d, j= %d, a = %d\n',i,j,a)
    division = ((-1).^j)*((x.^i)/(factorial(i)));  
    total=total+division;
end
result=1+total;
end