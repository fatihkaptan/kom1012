function e_x = my_exp(x)
total=0;
division=1;
i=1;
while division>10^(-6)   
    i=i+1;
    division = (x.^i)/(factorial(i));  
    total=total+division;
end
e_x=1+x+total;
end

%e^x = 1 + x + (x^2)/(2!) + (x^3)/(3!)+....