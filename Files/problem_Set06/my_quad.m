function [x1,x2,flag] = my_quad(a,b,c)
if a==0 & b==0 & c~=0
    flag=0;
    disp('no solution')
    x1=[];,x2=[];
    fprintf('x1 = %f, x2 = %f, flag = %d\n\n',x1,x2,flag)
    
elseif a==0 & b~=0
    flag=1;
    disp('one real root')
    root=-c/b;
    x1=root;,x2=[];
    fprintf('x1 = %f, x2 = %f, flag = %d\n\n',x1,x2,flag)
    
elseif a==0 & b==0 & c==0
    flag=99;
    disp('any x is a solution')
    x1=[];,x2=[];
    fprintf('x1 = %f, x2 = %f, flag = %d\n\n',x1,x2,flag)
    
else
    flag=2;
    delta=(b^2)-(4*a*c);
    x1 =  (-b + sqrt(delta))/2*a;
    x2 =  (-b - sqrt(delta))/2*a;
    if delta<0        
        disp('two complex roots')
        fprintf('x1 = %f+%fi, x2 = %f%fi, flag = %d\n\n',real(x1),imag(x1),real(x2),imag(x2),flag)
    else
        disp('two real roots')
        fprintf('x1 = %f, x2 = %f, flag = %d\n\n',x1,x2,flag)
    end        
end
end