%% problem_Set #6 
% 
% FUNCTIONS
%
% url = 'https://avesis.yildiz.edu.tr/lucun/dokumanlar'
%
%-*- coding: utf-8 -*-
% 
% Created on Tue Mar 31 18:33:08 2020
% 
% @author: Kaptan
%
% -*- run section shortcut : Ctrl+Return -*-
%if include input funciton. publish html outputs doesn't work!

%% 1.
clear all
clc

%{
Create a factorial function that prompts the user to enter a valid number 
until a nonnegative scalar number entered as the function input.
>faktoriyel.m
%}

%{
example output:

enter x: 5
factorial = 120

enter x: 0
factorial = 1

enter x: -5
breaking..

ans =

    'error, negative number!'
%}

%factorial program starts with=
%faktoriyel()

%% 2.
clear all
clc

%{
A store owner asks you to write a program for use in the checkout process. The program
should:
� Prompt the user to enter the cost of the first item.
� Continue to prompt for additional items, until the user enters 0.
� Display the total.
� Prompt for the amount the customer submits as payment.
� Display the change due
>store_func.m
%}

%{
example output:
enter the cost of first item: 23
enter the cost of 2. item> 12
enter the cost of 3. item> 11
enter the cost of 4. item> 0

bill =

    23    12    11

product entry finished, payment total of 3 item = 46.00

enter the customer payment amount > 50

50.00 - 46.00 = 4.00, please give 4.00 yang to customer
%}

%store program starts with=
%store_func()

%% 3.
clear all
clc

%{
Write your own MATLAB function to compute the exponential function 
directly from the Taylor series:
e^x = 1 + x + (x^2)/(2!) + (x^3)/(3!)+....

The series should end when the last term is less than 10^?6. 
Test your function against the built-in function exp, but be careful 
not to make x too large-this could cause rounding error.
>my_exp.m
%}

my_exp(1)
exp(1)

disp('----------')
x=1:5;
["my function" my_exp(x);"built-in function" exp(x)]'

%% 4.
clear all
clc

%{
Write a function
function [x1, x2, flag] = quad( a, b, c )

which computes the roots of the quadratic equation ax^2 +bx+c = 0. 
The input arguments a, b and c (which may take any values) are the 
coefficients of the quadratic, and x1, x2 are the two roots (if they exist),
which may be equal. The output argument flag must return the following values,
according to the number and type of roots:
� 0: no solution (a = b = 0, c ~= 0);
� 1: one real root (a = 0, b ~= 0, so the root is ?c/b);
� 2: two real or complex roots (which could be equal if they are real);
� 99: any x is a solution (a = b = c = 0).
>my_quad.m
%}
%flag0
[x1 x2 flag] = my_quad(0,0,1);
%flag1
[x1 x2 flag] = my_quad(0,-2,4);
%flag2
[x1 x2 flag] = my_quad(1,-3,2);
%flag2
[x1 x2 flag] = my_quad(1,-2,2);
%flag99
[x1 x2 flag] = my_quad(0,0,0);


%% 5.
clear all
clc

%{
Use the Taylor series
cos(x) = 1 ? x^2/2! + x^4/4! - x^6/6!+....
to write your own function to compute cos(x) correct to four decimal places
(x is in radians).
Test your function with the MATLAB function cos. 
Do not make x too large; that could cause rounding error.
>my_cos.m
%}
cos(2*pi)
my_cos(2*pi)
disp('______________')
x=-1:0.5:2; %as radian
["x" x;"my function" my_cos(x);"built-in function" cos(x)]'
